Build package
```sh
$ mvn package
```
Run some unit-tests
```sh
$ mvn test
```
Run app (in example on port 9091)
```sh
$ mvn exec:java -Dexec.mainClass=utill.stackowerfollow.SimpleHttpServer -Dexec.args="9091"
```
Now open page http://127.0.0.1:9091/ in WEB browser.