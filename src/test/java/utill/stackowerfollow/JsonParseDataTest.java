package utill.stackowerfollow;

import java.lang.reflect.Method;
import static org.junit.Assert.*;
import org.junit.Test;

public class JsonParseDataTest {

    
  @Test
  public void testFormatDate() throws Exception {
    Class underTest = JsonParseData.class;  
    Object obj=underTest.newInstance();  
    Method m = underTest.getDeclaredMethod("FormatDate",new Class[]{long.class});  
    m.setAccessible(true);
    String result = (String) m.invoke(obj, 1442249516); 
    assertEquals("2015-09-14 19:51:56 MSK",result);
    result = (String) m.invoke(obj, 123); 
    assertEquals("1970-01-01 03:02:03 MSK",result);
  }
  
  @Test
  public void testParceJson() throws Exception {
   JsonParseData obj = new JsonParseData(); 
   String invalidInput = "{\n" +
"    \"glossary\": {\n" +
"        \"title\": \"example glossary\",\n" +
"		\"GlossDiv\": {\n" +
"            \"title\": \"S\",\n" +
"			\"GlossList\": {\n" +
"                \"GlossEntry\": {\n" +
"                    \"ID\": \"SGML\",\n" +
"					\"SortAs\": \"SGML\",\n" +
"					\"GlossTerm\": \"Standard Generalized Markup Language\",\n" +
"					\"Acronym\": \"SGML\",\n" +
"					\"Abbrev\": \"ISO 8879:1986\",\n" +
"					\"GlossDef\": {\n" +
"                        \"para\": \"A meta-markup language, used to create markup languages such as DocBook.\",\n" +
"						\"GlossSeeAlso\": [\"GML\", \"XML\"]\n" +
"                    },\n" +
"					\"GlossSee\": \"markup\"\n" +
"                }\n" +
"            }\n" +
"        }\n" +
"    }\n" +
"}";
   String result = obj.ParseJson(invalidInput);
   assertEquals("Can't parse reply from server",result);         
   result = obj.ParseJson("foobar");
   assertEquals("Can't parse reply from server",result);
   
   String validInput = "{\"items\":[{\"tags\":[\"scala\",\"erlang\",\"distributed-computing\",\"actor\"],\"owner\":{\"reputation\":1562,\"user_id\":391411,\"user_type\":\"registered\",\"accept_rate\":86,\"profile_image\":\"https://www.gravatar.com/avatar/3de2d5c3d7af24ae0b8575dcb1b138e8?s=128&d=identicon&r=PG\",\"display_name\":\"Varun Madiath\",\"link\":\"http://stackoverflow.com/users/391411/varun-madiath\"},\"is_answered\":true,\"view_count\":247,\"answer_count\":2,\"score\":5,\"last_activity_date\":1329870473,\"creation_date\":1329861386,\"question_id\":9385834,\"link\":\"http://stackoverflow.com/questions/9385834/erlang-scala-migrating-actors-from-one-node-to-another\",\"title\":\"Erlang/Scala Migrating Actors from one node to another\"},{\"tags\":[\"ruby\",\"scala\",\"erlang\",\"parallel-processing\",\"distributed-transactions\"],\"owner\":{\"reputation\":2503,\"user_id\":727646,\"user_type\":\"registered\",\"accept_rate\":71,\"profile_image\":\"https://www.gravatar.com/avatar/f4fdd90812e57dfebb94bff4e395c8ea?s=128&d=identicon&r=PG\",\"display_name\":\"chrispanda\",\"link\":\"http://stackoverflow.com/users/727646/chrispanda\"},\"is_answered\":true,\"view_count\":774,\"accepted_answer_id\":7758770,\"answer_count\":1,\"score\":5,\"last_activity_date\":1318564951,\"creation_date\":1318529311,\"last_edit_date\":1318564951,\"question_id\":7758474,\"link\":\"http://stackoverflow.com/questions/7758474/distributed-transactions-and-queues-ruby-erlang-scala\",\"title\":\"distributed transactions and queues, ruby, erlang, scala\"}],\"has_more\":false,\"quota_max\":300,\"quota_remaining\":297}";
   result = obj.ParseJson(validInput);
   assertNotNull(result); 
}

  
}


