package utill.stackowerfollow;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.Iterator;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class JsonParseData {
    /** Variable for page navigation (like "Next|Prev" buttons) (parameter "page" in url ) - don't used now  */
    private boolean hasMore = false;
    /** Wrapper for json parameters */
    private class WrapperElement {
        
       public String CreationDate;
       public String Title;
       public boolean IsAnswered;
       public String Link;
       public String Author;
       
       WrapperElement(String iCreationDate, String iTitle,
               boolean iIsAnswered, String iLink, String iAuthor){
           CreationDate = iCreationDate;
           Title = iTitle;
           IsAnswered = iIsAnswered;
           Link = iLink;
           Author = iAuthor;
       }
    }
    
    /** Decode timestamp to string */
    private String FormatDate(long creationDate){
        Date date = new Date(creationDate*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z"); // the format of your date
        return sdf.format(date);
    }
    
    /** Prepare HTML data with search results as table */
    private String PrepareHTMLFromWrapper(ArrayList<WrapperElement> elements){
    Iterator i = elements.iterator();
    String retHTML =  "<table>\n" +
                    "<thead>\n" +
                    "<tr>\n" +
                    "<th>Search result ... (first " + String.valueOf(elements.size()) + " records) </th>\n" +
                    "</tr>\n" +
                    "</thead><tbody>";
    while (i.hasNext()) {
        WrapperElement Element = (WrapperElement) i.next(); 
        retHTML += Element.IsAnswered?  "<tr class=\"ans\">" : "<tr>"; 
        retHTML += "<td>Creation date: " + Element.CreationDate + "<br>" + 
                "Author: " + Element.Author + "<br>" + 
                "Title: " + Element.Title + "<br>" + 
                "Source page: <a href=\"" + Element.Link + "\" target=\"_blank\">Link</a></td></tr>";
        }
    return retHTML + "</tbody></table>";
    }
    
    /** Try parce JSON to wrapper and return HTML content */
    public String ParseJson(String jsonString) throws IOException {
    String dataHTML = "Can't parse reply from server";
    try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(jsonString);
            /** If "items" key is prowided - parce data, else - we can't parce reply*/
            if (jsonObject.containsKey("items")) {
                JSONArray items= (JSONArray) jsonObject.get("items");
                hasMore = (boolean) jsonObject.get("has_more");
                Iterator i = items.iterator();
                ArrayList<WrapperElement> elements = new ArrayList<WrapperElement>();
                while (i.hasNext()) {
                    JSONObject innerObj = (JSONObject) i.next();
                    JSONObject ownerObj = (JSONObject) innerObj.get("owner");
                    elements.add( 
                            new WrapperElement(
                                    FormatDate((long) innerObj.get("creation_date")),
                                    (String)innerObj.get("title"),
                                    (boolean) innerObj.get("is_answered"),
                                    (String) innerObj.get("link"),
                                    (String) ownerObj.get("display_name")) );
                }
                dataHTML = PrepareHTMLFromWrapper(elements);
            }
        } catch (ParseException ex) {
            ex.printStackTrace();
        } catch (NullPointerException ex) {
            dataHTML = ex.getMessage();
        }
    return dataHTML;
    }
}
