/** Make request to api.stackexchange.com */
package utill.stackowerfollow;

import java.net.*;
import java.io.*;
import java.util.zip.GZIPInputStream;
import java.nio.charset.Charset;

public class CurlRequestSourceData {
    /** Base request URL */
    private static String requestUrl = "http://api.stackexchange.com/2.2/search?pagesize=100&order=desc&sort=creation&site=stackoverflow"; // ширина коробки
    
    private static String readJSONPage(BufferedReader rd) throws IOException {
           StringBuilder sb = new StringBuilder();
           String inputLine;
           while ((inputLine = rd.readLine()) != null){
               sb.append(inputLine);
           }
           return sb.toString();
    }
    
    
    public static String HTMLData(String page, String keywords) throws Exception {
        /** Add to base URL keywords and page parameters */
        String request = requestUrl + "&intitle=" + keywords + "&page=" + page; 
        String parsedData = "";
        /** Try create request */
        try {
            InputStream is = new URL(request).openStream();
            try {
              BufferedReader rd = new BufferedReader(new InputStreamReader(new GZIPInputStream(is), Charset.forName("UTF-8")));
              /** Receive json text from responce */
              String jsonText = readJSONPage(rd);
              /** Parse it and return as HTML */
              parsedData = new JsonParseData().ParseJson(jsonText);
            } finally {
              is.close();
            }
        } catch (Exception ex) {
             /** If something is wrong - reply error mesage as content*/
            parsedData = ex.getMessage();
        }
        return parsedData;
    }
}

