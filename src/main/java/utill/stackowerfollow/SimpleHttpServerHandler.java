/** HTTP server handler */

package utill.stackowerfollow;

import io.netty.channel.ChannelInboundHandlerAdapter;
import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.*;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.QueryStringDecoder;
import io.netty.util.CharsetUtil;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class SimpleHttpServerHandler extends ChannelInboundHandlerAdapter {
     private HttpRequest request;
     /** HTML header with CSS for result table and JS for parce URL parameters */
     private static String HTMLHeader = "<!DOCTYPE html>"
             + "<html><head><title>Search Questions</title>"
             + "<style type=\"text/css\">\n"
             + "table{\n"
             + "width: 60%;\n"
             + "border: 1px solid #000;\n"
             + "border-collapse: collapse;\n"
             + "font: normal 10pt Arial, Helvetica, sans-serif;\n"
             + "color: #000;\n"
             + "text-align: left;\n"
             + "}\n"
             + "td{\n"
             + "border: 1px solid #000;\n"
             + "padding: 2px;\n"
             + "}\n"
             + "th{\n"
             + "background: #9cf;\n"
             + "text-align: center;\n"
             + "}\n" 
             + "tr.ans{\n"
             + "background: #9cf;\n"
             + "}\n"
             + "</style>"
             + "<script type=\"text/javascript\">\n"
             + "window.onload=function() {\n"
             + "var data=location.search;\n" 
             + "if(data) {\n" 
             + "    data=location.search.substring(1);\n" 
             + "    data=data.split('&');\n" 
             + "    var pairs={};\n" 
             + "    for(var i=0; i<data.length; i++){\n" 
             +"        var tmp=data[i].split('=');\n" 
             + "        pairs[tmp[0]]=tmp[1];\n" 
             + "    	}\n" 
             + "    var f = document.searchform;\n" 
             + "    for (var i in pairs) {\n" 
             + "        if(f.elements[i]) {f.elements[i].value = unescape(pairs[i].replace('+', ' '));}\n" 
             + "        }\n" 
             + "    }\n" 
             + "};\n" 
             + "</script>"
             + "</head><body>";
     /** Simple HTML input search form */
     private static String HTMLSearchForm = "<form name=\"searchform\" action=\"/\" method=\"get\">\n" +
                            "  <input type=\"text\" name=\"keywords\" title=\"Example: scala,erlang\"  value=\"\">\n" +
                            "  <input type=\"submit\" value=\"Search...\">\n" +
                            "</form> <br>";
     /** HTML footer */
     private static String HTMLFooter = "</body></html>";
     
     /** Buffer that stores the response content */
     private final StringBuilder buf = new StringBuilder();
     static CurlRequestSourceData pHTTPRequest;
     
     SimpleHttpServerHandler(CurlRequestSourceData pRequest) {
        pHTTPRequest = pRequest;
    }
     @Override
     public void channelReadComplete(ChannelHandlerContext ctx) {
         ctx.flush();
     }
 
     @Override
     public void channelRead(ChannelHandlerContext ctx, Object msg) {
         /** New request 
         * Variable "page" for page navigation 
         * (like "Next|Prev" buttons) 
         * (parameter "page" in url ) - don't used now
         * "keywords" - searching string (will be passed from url)
         */
         String keywords = "";  
         String page = "";
         if (msg instanceof HttpRequest) {
             HttpRequest request = this.request = (HttpRequest) msg;
 
             if (HttpHeaders.is100ContinueExpected(request)) {
                 send100Continue(ctx);
             }
             /** Create reply with content */
             buf.setLength(0);
             buf.append(HTMLHeader);
             buf.append(HTMLSearchForm);
             /** Try extract parameters from URL (keywords and page)*/
             QueryStringDecoder queryStringDecoder = new QueryStringDecoder(request.getUri());
             Map<String, List<String>> params = queryStringDecoder.parameters();
             if (!params.isEmpty()) {
                 for (Entry<String, List<String>> p: params.entrySet()) {
                     String key = p.getKey();
                     List<String> vals = p.getValue();
                     for (String val : vals) {
                            if (key.equals("keywords")) {
                                keywords = val.replaceAll("\\+|\" \"|\\-|\\s", "");
                            } 
                            else if (key.equals("page")) {
                                page = val;
                            }
                     }
                 }
             }
             
         }
         /** Prepare content for responce */
         String preparedContent = "";
         if (keywords.isEmpty()) {
             /** If "keywords" parameter not presented in URL - reply this mesage as content*/
             preparedContent +="Please, type the keywords into the form (Example: \"scala,erlang\")";
         }
         else{
             /** If "keywords" is presented - try get content from HTTP request to API*/
         try {
            preparedContent = pHTTPRequest.HTMLData(page, keywords);
         } 
         catch(Exception ex){
             /** If something is wrong - reply error mesage as content*/
            preparedContent = ex.getMessage();
         }
         }
         /** Add to responce prepared content and HTML footer */
         buf.append(preparedContent);
         buf.append(HTMLFooter);
         if (!writeResponse(ctx)) {
            /** If keep-alive is off, close the connection once the content is fully written. */
            ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
         }
     }

     private boolean writeResponse(ChannelHandlerContext ctx) {
         /** Decide whether to close the connection or not. */
         boolean keepAlive = HttpHeaders.isKeepAlive(request);
         /** Build the response object. */
         FullHttpResponse response = new DefaultFullHttpResponse(
                 HTTP_1_1, OK,
                 Unpooled.copiedBuffer(buf.toString(), CharsetUtil.UTF_8));
         if (keepAlive) {
             /** Add 'Content-Length' header only for a keep-alive connection. */
             response.headers().set(CONTENT_LENGTH, response.content().readableBytes());
             /** Add keep alive header as per:
             * - http://www.w3.org/Protocols/HTTP/1.1/draft-ietf-http-v11-spec-01.html#Connection */
             response.headers().set(CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
         }
         /** Write the response. */
        ctx.write(response);
        return keepAlive;
     }
 
     private static void send100Continue(ChannelHandlerContext ctx) {
         FullHttpResponse response = new DefaultFullHttpResponse(HTTP_1_1, CONTINUE);
         ctx.write(response);
     }
 
     @Override
     public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
         cause.printStackTrace();
         ctx.close();
     }
 }
